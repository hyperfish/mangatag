import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';

import { ElectronService } from 'ngx-electron';
import { AppdataService } from '@services/appdata.service';
import { KitsuService } from '@services/kitsu.service';

import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field'
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input'
import { MatDialogModule } from '@angular/material/dialog'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SpinnerDialogComponent } from './spinner-dialog/spinner-dialog.component';
import { TagDialogComponent } from './tag-dialog/tag-dialog.component';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SpinnerDialogComponent,
    TagDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule,
    FormsModule,
    MatCardModule,
    MatDialogModule
  ],
  providers: [ElectronService, AppdataService, KitsuService,],
  bootstrap: [AppComponent],
  entryComponents: [SpinnerDialogComponent, TagDialogComponent]
})
export class AppModule { }
