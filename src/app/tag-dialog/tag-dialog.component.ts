import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { KitsuService } from '@services/kitsu.service';

@Component({
  selector: 'app-tag-dialog',
  templateUrl: './tag-dialog.component.html',
  styleUrls: ['./tag-dialog.component.scss']
})
export class TagDialogComponent implements OnInit {

  tagInput: string;
  mangaTitle: string;
  mangaSynopsis: string;

  constructor(public dialogRef: MatDialogRef<TagDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private kitsuService: KitsuService,
  ) { }

  ngOnInit() {
    console.log('data', this.data)
    this.tagInput = this.data._tags;
    this.mangaTitle = this.data._title;
    this.mangaSynopsis = this.data._synopsis
  }

  onSave() {
    console.log('updated form with: ', this.data._id + ' ' + this.tagInput)
    this.kitsuService.insertTagEntry(this.data._id, this.tagInput)
    this.dialogRef.close();
  }

  onCancel() {
    this.dialogRef.close();
  }

}
