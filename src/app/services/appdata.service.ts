import {Injectable} from '@angular/core'
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
import { catchError, retry, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })

export class AppdataService {
    httpHeaders: HttpHeaders;
    error: string;

    constructor(private http: HttpClient) {

    }   

    httpGetRequest(url) {
        try {
          // console.log('url', url)
          return this.http.get<any>('https://kitsu.io/api/edge/' + url)
            .pipe(
              retry(3), // retry a failed request up to 3 times
            );
        } catch (error) {
          console.log('error', error);
          return null;
        }
    }

    httpGetRequestFull(url) {
      try {
        // console.log('url', url)
        return this.http.get<any>(url)
          .pipe(
            retry(3), // retry a failed request up to 3 times
          );
      } catch (error) {
        console.log('error', error);
        return null;
      }
    }

}