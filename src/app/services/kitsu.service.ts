import { Injectable } from '@angular/core';
import { Manga } from '@models/manga';

@Injectable({
	providedIn: 'root'
})

export class KitsuService {

	private userID: string;
	private mangaList: Manga[];

	constructor() {
		this.mangaList = [];
	}

	public insertTagEntry(id: string, tag: string) {
		for (var manga of this.mangaList) {
			if (id === manga.id) {
				manga.tags = tag;
			}
		}
	}

	public getUserID() {
		return this.userID;
	}

	public setUserID(id: string) {
		this.userID = id;
		console.log('user id', this.userID)
	}

	public insertMangaEntry(manga: Manga) {
		this.mangaList.push(manga);
	}

	public getMangaList() {
		return this.mangaList;
	}

	public insertMangaList(mangaArr: Manga[]) {
		this.mangaList = mangaArr;
	}
}