export class Manga {

  private _title: string;
  private _synopsis: string;
  private _coverArt: string;
  private _id: string;
  private _tags: string;
  private _progress: number;

  constructor(title, synopsis, coverArt, id, progress) {
    this._title = title;
    this._synopsis = synopsis;
    this._coverArt = coverArt;
    this._id = id;
    this._tags = '';
    this._progress = progress;
  }

  public getManga() {
    let manga = { title: this._title, synopsis: this._synopsis, coverArt: this._coverArt, tags: this._tags };
    return manga;
  }

  public getMangaID() {
    return this._id;
  }

  get title(): string {
    return this._title;
  }

  get synopsis(): string {
    return this._synopsis;
  }

  get coverArt(): string {
    return this._coverArt;
  }

  get tags(): string {
    return this._tags;
  }

  set tags(tag: string) {
    this._tags = tag;
  }

  get id(): string {
    return this._id;
  }

  get progress(): number {
    return this._progress
  }

}