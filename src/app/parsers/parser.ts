import * as papaparser from 'papaparse'
import { Manga } from '@models/manga';

export class Parser {

	public parse(files) {
		var mangaList: Manga[] = [];
		var flag: boolean = false;
		if (files[0]) {
			console.log(files[0]);
			return new Promise((resolve, reject) => 
				papaparser.parse(files[0], {
					delimiter: "|",
					complete: (res, file) => {
						for (var i = 1; i < res.data.length - 1; i++) {
							let mangaEntry = res.data[i];
							if (mangaEntry[3]) { mangaEntry[3] = mangaEntry[3].trim() }
							let newManga = new Manga(mangaEntry[1], mangaEntry[2], mangaEntry[3], mangaEntry[0], mangaEntry[4])
							newManga.tags = mangaEntry[4];
							mangaList.push(newManga);
						}
						resolve(mangaList);
					}
				}
			))
		}
	}
}