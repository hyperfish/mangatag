import { saveAs } from 'file-saver';
import { Manga } from '../models/manga';

export class Serializer {

	public serialize(mangaList) {
		var mangaOut: string = 'ID | Title | Synopsis | Cover Art | Tags | Progress\n';
		for (var manga of mangaList) {
			mangaOut = mangaOut + `${manga.id.trim()} | ${manga.title.trim()} | ${manga.synopsis.trim()} | ${manga.coverArt.trim()} | ${manga.tags.trim()} | ${manga.progress.toString()}\n`
		}
		var file = new Blob([mangaOut], { type: "text/text;charset=utf-8" });
		saveAs(file, 'test.txt')
	}
}