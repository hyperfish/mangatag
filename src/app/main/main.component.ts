import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import { AppdataService } from '@services/appdata.service'
import { KitsuService } from '@services/kitsu.service';
import { Manga } from '@models/manga';
import { MatDialog, MatDialogRef } from '@angular/material';
import { SpinnerDialogComponent } from '../spinner-dialog/spinner-dialog.component';
import { TagDialogComponent } from '../tag-dialog/tag-dialog.component';
// import { saveAs } from 'file-saver';
// import * as parser from 'papaparse'
import { Serializer } from '../parsers/serializer';
import { Parser } from '../parsers/parser';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})

export class MainComponent implements OnInit {

  mangas: Manga[] = [];

  displayedColumns: string[] = ['title', 'progress', 'coverArt', 'tags'];
  dataSource: Manga[];

  constructor(private _electronService: ElectronService,
    private appdataService: AppdataService,
    private kitsuService: KitsuService,
    private ref: ChangeDetectorRef,
    private _dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  onClickMe() {
    this.getUserID('hyperfish')
    this.openSpinnerDialog()
  }

  onOpenTagDialog(e) {
    const tagDialog = this._dialog.open(TagDialogComponent,
      { width: '600px', data: e });
  }

  onChange(files: File[]) {
    var parser = new Parser();
    parser.parse(files).then((res) => this.repopulateTableData(res));
  }

  repopulateTableData(mangaList) {
    this.kitsuService.insertMangaList(mangaList);
    this.dataSource = this.kitsuService.getMangaList();
    this.ref.detectChanges();
  }

  onWriteFileButton() {
    // var mangaList = this.kitsuService.getMangaList();
    // var mangaOut: string = 'ID | Title | Synopsis | Cover Art | Tags | Progress\n';
    // for (var manga of mangaList) {
    //   mangaOut = mangaOut + `${manga.id.trim()} | ${manga.title.trim()} | ${manga.synopsis.trim()} | ${manga.coverArt.trim()} | ${manga.tags.trim()} | ${manga.progress.toString()}\n`
    // }
    // var file = new Blob([mangaOut], { type: "text/text;charset=utf-8" });
    // saveAs(file, 'test.txt')
    var serializer = new Serializer()
    serializer.serialize(this.kitsuService.getMangaList());
  }

  private async getUserID(name) {
    let url = 'users?filter[name]=' + name;
    await this.appdataService.httpGetRequest(url).subscribe(res => {
      this.kitsuService.setUserID(res.data[0].id);
      this.getLibraryEntries(res.data[0].id)
    })
  }

  // loop get library entries
  private async getLibraryEntries(id) {
    let url = 'users/' + id + '/library-entries';
    await this.appdataService.httpGetRequest(url).subscribe(res => {

      for (var entry of res.data) {
        this.getLibraryEntryManga(entry.id, entry.attributes.progress);
      }

      if (res.links.next) {
        this.goNextPageAndGetLibraryEntries(res.links.next);
      } else {
        console.log('done:', this.kitsuService.getMangaList());
      }

    })
  }

  // go to next page and loop get single entry
  private async goNextPageAndGetLibraryEntries(url: string) {
    this.appdataService.httpGetRequestFull(url).subscribe(res => {
      for (var entry of res.data) {
        this.getLibraryEntryManga(entry.id, entry.attributes.progress);
      }

      if (res.links.next) {
        this.goNextPageAndGetLibraryEntries(res.links.next);
      } else {
        console.log('done:', this.kitsuService.getMangaList());
        this.mangas = this.kitsuService.getMangaList();
        this.dataSource = this.mangas;
        this.ref.detectChanges();
        this._dialog.closeAll();
      }
    })
  }

  // get single library entry
  private async getLibraryEntryManga(id, progress) {
    let url = 'library-entries/' + id + '/manga'
    await this.appdataService.httpGetRequest(url).subscribe(res => {
      if (res.data) {
        let attributes = res.data.attributes;
        // there are some new line stuff in synopsis for some reason
        let newManga: Manga = new Manga(attributes.canonicalTitle, attributes.synopsis.replace(/\r?\n|\r|↵/g, ''), attributes.posterImage.tiny, res.data.id, progress);
        this.kitsuService.insertMangaEntry(newManga);
      } else {
        return;
      }
    })
  }

  private openSpinnerDialog() {
    let dialogRef: MatDialogRef<SpinnerDialogComponent> = this._dialog.open(SpinnerDialogComponent, {
      panelClass: 'transparent',
      disableClose: true
    })
  }
}
