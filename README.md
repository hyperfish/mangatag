# MangaTag

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Build (electron)

Run `npm run start:electron` to build and run app. Devtools are enabled and can be commented off

## Loading into spreadsheet

Used | as a delimiter.

## Changelog
<details>
    <summary>v0.0.1</summary>
    1. Added tag inputs.<br>
    2. Fixed angular material not showing theme.<br>
    3. Added progress spinner for loading in library.<br>
</details>

<details>
    <summary>v0.0.2</summary>
    1. Added in button to write out to txt file.<br>
    2. Cleaned data as synopsis have carriage returns before and after sources.<br>
</details>

<details>
    <summary>v0.0.3</summary>
    1. Changed tag update to a dialog.<br>
    2. Tags can now be updated properly.<br>
</details>

<details>
    <summary>v0.0.4</summary>
    1. Added in button to read in txt or csv file.<br>
    2. Tag dialog now shows synopsis.<br>
</details>

<details>
    <summary>v0.0.5</summary>
    1. Shift file read in and out to its own classes.<br>
    2. Table now shows chapter progression.<br>
    3. Synopsis will be shown in dialog instead of table.<br>
</details>